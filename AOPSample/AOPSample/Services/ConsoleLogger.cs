namespace AOPSample.Services
{
    using System;
    using Interfaces;
    
    internal sealed class ConsoleLogger : ILogger
    {
        public void Debug(string text)
        {
            Console.WriteLine(text);
        }
    }
}