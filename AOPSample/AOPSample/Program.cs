﻿namespace AOPSample
{
    using System;
    using Microsoft.Practices.Unity;
    using Interfaces;

    internal class Program
    {
        public static void Main(string[] args)
        {
            ContainerHelper.RegisterServices();

            var sampleService = ContainerHelper.Container.Resolve<ISampleService>();

            var kv1 = sampleService.GetKeyValueString("Name", "Bob");
            var sum = sampleService.Sum(1, 2);

            try
            {
                var kv2 = sampleService.GetKeyValueString(null, "Bob");
            }
            catch(Exception ex)
            {
                Console.WriteLine($"Exception catched: {ex.GetType().FullName} - {ex.Message}");
            }

            Console.WriteLine("Press Enter to exit...");
            Console.ReadLine();
        }
    }
}