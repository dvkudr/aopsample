namespace AOPSample.Interfaces
{
    public interface ISampleService
    {
        string GetKeyValueString(string s1, string s2);
        int Sum(int n1, int n2);
    }
}