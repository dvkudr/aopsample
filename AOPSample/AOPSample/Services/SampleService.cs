namespace AOPSample.Services
{
    using System;
    using AOP;
    using Interfaces;
  
    internal sealed class SampleService : ISampleService 
    {
        [Log]
        public string GetKeyValueString(string key, string value)
        {
            if (string.IsNullOrEmpty(key)) throw new ArgumentException(nameof(key));
            if (string.IsNullOrEmpty(value)) throw new ArgumentException(nameof(value));
            
            return $"{key} = {value}";
        }

        [Log]
        public int Sum(int n1, int n2)
        {
            return n1 + n2;
        }
    }
}