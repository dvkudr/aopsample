namespace AOPSample.Interfaces
{
    public interface ILogger
    {
        void Debug(string text);
    }
}