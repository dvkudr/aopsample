namespace AOPSample
{
    using System;
    using Microsoft.Practices.Unity;
    using Microsoft.Practices.Unity.InterceptionExtension;
    using AOP;
    using Interfaces;
    using Services;

    internal static class ContainerHelper
    {
        public static IUnityContainer Container;

        static ContainerHelper()
        {
            Container = new UnityContainer();
            Container.AddNewExtension<Interception>();
        }

        public static void RegisterServices()
        {
            Container.RegisterType<ILogCallHandler, LogCallHandler>();

            Container.RegisterType<ILogger, ConsoleLogger>();

            Container.RegisterType<ISampleService, SampleService>().Configure<Interception>()
                .SetInterceptorFor<ISampleService>(new InterfaceInterceptor());
        }
    }
}