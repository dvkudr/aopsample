namespace AOPSample.AOP
{
    using System.Linq;
    using Microsoft.Practices.Unity.InterceptionExtension;
    using Interfaces;
    
    internal sealed class LogCallHandler : ILogCallHandler
    {
        private readonly ILogger _log;
        
        public LogCallHandler(ILogger log)
        {
            _log = log;
        }
        
        public IMethodReturn Invoke(IMethodInvocation input, GetNextHandlerDelegate getNext)
        {
            _log.Debug($"{nameof(LogCallHandler)}: invoking method - {input.Target}.{input.MethodBase.Name}({string.Join(", ", input.Arguments.Cast<object>())})");

            var invokeResult = getNext().Invoke(input, getNext);

            _log.Debug(invokeResult.Exception != null
                ? $"{nameof(LogCallHandler)}: exception was thrown - {invokeResult.Exception.GetType()} - {invokeResult.Exception.Message}"
                : $"{nameof(LogCallHandler)}: returned value - {invokeResult.ReturnValue}");

            return invokeResult;
        }

        public int Order { get; set; }
    }
}